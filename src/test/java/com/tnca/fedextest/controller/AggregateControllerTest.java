package com.tnca.fedextest.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tnca.fedextest.model.Aggregate;
import com.tnca.fedextest.service.AggregateService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static com.tnca.fedextest.TestUtil.readFileAsString;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({AggregateController.class})
class AggregateControllerTest {
    @MockBean
    private AggregateService aggregateService;

    @Autowired
    private MockMvc mockMvc;

    @Captor
    private ArgumentCaptor<ArrayList<String>> priceCaptor;

    @Captor
    private ArgumentCaptor<ArrayList<String>> trackCaptor;

    @Captor
    private ArgumentCaptor<ArrayList<String>> shipmentsCaptor;

    @Test
    public void allQueryParams() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        JsonNode trackJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        JsonNode shipmentJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));

        Aggregate response = new Aggregate(pricingJson, trackJson, shipmentJson);
        Mockito.when(aggregateService.aggregate(any(), any(), any())).thenReturn(response);

        mockMvc.perform(
                get("/aggregation")
                        .accept(APPLICATION_JSON)
                        .param("pricing", "NL", "BE")
                        .param("track", "123456789", "987654321")
                        .param("shipments", "123456789", "987654321")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pricing", notNullValue()))
                .andExpect(jsonPath("$.track", notNullValue()))
                .andExpect(jsonPath("$.shipments", notNullValue()))
        ;

        verify(aggregateService, times(1)).aggregate(priceCaptor.capture(), trackCaptor.capture(), shipmentsCaptor.capture());
        assertLinesMatch(priceCaptor.getValue(), List.of("NL", "BE"));
        assertLinesMatch(trackCaptor.getValue(), List.of("123456789", "987654321"));
        assertLinesMatch(shipmentsCaptor.getValue(), List.of("123456789", "987654321"));
    }

    @Test
    public void onlyPricing() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));

        Aggregate response = new Aggregate(pricingJson, null, null);
        Mockito.when(aggregateService.aggregate(any(), any(), any())).thenReturn(response);

        mockMvc.perform(
                get("/aggregation")
                        .accept(APPLICATION_JSON)
                        .param("pricing", "NL", "BE")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pricing", notNullValue()))
                .andExpect(jsonPath("$.track", nullValue()))
                .andExpect(jsonPath("$.shipments", nullValue()))
        ;

        verify(aggregateService, times(1)).aggregate(priceCaptor.capture(), eq(null), eq(null));
        assertLinesMatch(priceCaptor.getValue(), List.of("NL", "BE"));
    }

    @Test
    public void onlyPricingAndShipment() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        JsonNode shipmentJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));

        Aggregate response = new Aggregate(pricingJson, null, shipmentJson);
        Mockito.when(aggregateService.aggregate(any(), any(), any())).thenReturn(response);

        mockMvc.perform(
                get("/aggregation")
                        .accept(APPLICATION_JSON)
                        .param("pricing", "NL", "BE")
                        .param("shipments", "123456789", "987654321")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pricing", notNullValue()))
                .andExpect(jsonPath("$.track", nullValue()))
                .andExpect(jsonPath("$.shipments", notNullValue()))
        ;

        verify(aggregateService, times(1)).aggregate(priceCaptor.capture(), eq(null), shipmentsCaptor.capture());
        assertLinesMatch(priceCaptor.getValue(), List.of("NL", "BE"));
        assertLinesMatch(shipmentsCaptor.getValue(), List.of("123456789", "987654321"));
    }

    @Test
    public void wrongLengthInputs() throws Exception {
        mockMvc.perform(
                get("/aggregation")
                        .accept(APPLICATION_JSON)
                        .param("pricing", "1")
                        .param("track", "12345678")
                        .param("shipments", "12345678")
        )
                .andExpect(status().isBadRequest())
        ;
    }
}