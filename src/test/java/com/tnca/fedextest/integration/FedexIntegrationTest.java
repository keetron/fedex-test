package com.tnca.fedextest.integration;

import com.tnca.fedextest.FedexTestApplication;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = FedexTestApplication.class)
class FedexIntegrationTest {

    @LocalServerPort
    Integer randomServerPort = 0;

    @Test
    void pricingAndShipments() {
        given()
                .port(randomServerPort)
                .queryParam("pricing", List.of("NL", "DE", "RU", "BE", "LU"))
                .queryParam("shipments", List.of("123456781", "123456782", "123456783", "123456784", "123456785"))
                .contentType(ContentType.JSON)
                .when()
                .get("/aggregation").prettyPeek()
                .then()
                .statusCode(200)
                .body("pricing", notNullValue())
                .body("track", nullValue())
                .body("shipments", notNullValue());
    }

}
