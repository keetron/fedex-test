package com.tnca.fedextest.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tnca.fedextest.client.PricingClient;
import com.tnca.fedextest.client.ShipmentClient;
import com.tnca.fedextest.client.TrackClient;
import com.tnca.fedextest.model.Aggregate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static com.tnca.fedextest.TestUtil.readFileAsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AggregateServiceTest {
    @Mock
    private PricingClient pricingClient;

    @Mock
    private TrackClient trackClient;

    @Mock
    private ShipmentClient shipmentClient;

    @Captor
    private ArgumentCaptor<ArrayList<String>> priceCaptor;

    @Captor
    private ArgumentCaptor<ArrayList<String>> trackCaptor;

    @Captor
    private ArgumentCaptor<ArrayList<String>> shipmentsCaptor;

    private AggregateService aggregateService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        aggregateService = new AggregateService(pricingClient, shipmentClient, trackClient);
    }

    @Test
    void aggregateAll() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        JsonNode trackJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        JsonNode shipmentJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(pricingClient.fetchPricing(any())).thenReturn(pricingJson);
        when(trackClient.fetchTrack(any())).thenReturn(trackJson);
        when(shipmentClient.fetchShipment(any())).thenReturn(shipmentJson);

        Aggregate result = aggregateService.aggregate(
                List.of("1", "2", "3", "4", "5"),
                List.of("1", "2", "3", "4", "5"),
                List.of("1", "2", "3", "4", "5"));

        assertEquals(pricingJson, result.getPricing());
        assertEquals(trackJson, result.getTrack());
        assertEquals(shipmentJson, result.getShipments());

        verify(pricingClient, times(1)).fetchPricing(any());
        verify(trackClient, times(1)).fetchTrack(any());
        verify(shipmentClient, times(1)).fetchShipment(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

    @Test
    void onlyShipments() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode shipmentJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(shipmentClient.fetchShipment(any())).thenReturn(shipmentJson);

        Aggregate result = aggregateService.aggregate(null, List.of(), List.of("1", "2", "3", "4", "5"));

        assertEquals(shipmentJson, result.getShipments());

        verify(shipmentClient, times(1)).fetchShipment(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

    @Test
    void onlyTrack() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode trackJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(trackClient.fetchTrack(any())).thenReturn(trackJson);

        Aggregate result = aggregateService.aggregate(List.of(), List.of("1", "2", "3", "4", "5"), null);

        assertEquals(trackJson, result.getTrack());

        verify(trackClient, times(1)).fetchTrack(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

    @Test
    void onlyPricing() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(pricingClient.fetchPricing(any())).thenReturn(pricingJson);

        Aggregate result = aggregateService.aggregate(List.of("1", "2", "3", "4", "5"), null, List.of());
        assertEquals(pricingJson, result.getPricing());

        verify(pricingClient, times(1)).fetchPricing(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

    @Test
    void onlyShipmentsHitsTimer() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode shipmentJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(shipmentClient.fetchShipment(any())).thenReturn(shipmentJson);

        Aggregate result = aggregateService.aggregate(null, List.of(), List.of("1", "2", "3"));

        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);

        Thread.sleep(5000);

        result = aggregateService.aggregate(null, List.of(), List.of("4"));

        assertEquals(shipmentJson, result.getShipments());

        verify(shipmentClient, times(1)).fetchShipment(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

    @Test
    void onlyTrackHitTimer() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode trackJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(trackClient.fetchTrack(any())).thenReturn(trackJson);

        Aggregate result = aggregateService.aggregate(List.of(), List.of("1", "2", "3"), null);

        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);

        Thread.sleep(5000);

        result = aggregateService.aggregate(List.of(), List.of("4"), null);

        assertEquals(trackJson, result.getTrack());

        verify(trackClient, times(1)).fetchTrack(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

    @Test
    void onlyPricingHitTimer() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(pricingClient.fetchPricing(any())).thenReturn(pricingJson);

        Aggregate result = aggregateService.aggregate(List.of("NL", "BE", "DE"), null, List.of());
        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());

        Thread.sleep(5000L);

        result = aggregateService.aggregate(List.of("RU"), null, List.of());
        assertEquals(pricingJson, result.getPricing());

        verify(pricingClient, times(1)).fetchPricing(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

    @Test
    void onlyShipmentsInTwoRequests() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode shipmentJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(shipmentClient.fetchShipment(any())).thenReturn(shipmentJson);

        Aggregate result = aggregateService.aggregate(null, null, List.of("1", "2", "3"));
        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());

        result = aggregateService.aggregate(null, null, List.of("4", "5"));
        assertEquals(shipmentJson, result.getShipments());
        assertNull(result.getTrack());
        assertNull(result.getPricing());

        verify(shipmentClient, times(1)).fetchShipment(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);

        verify(shipmentClient, times(1)).fetchShipment(shipmentsCaptor.capture());
        assertLinesMatch(shipmentsCaptor.getValue(), List.of("1", "2", "3", "4", "5"));
    }

    @Test
    void onlyTrackInTwoRequests() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode trackJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(trackClient.fetchTrack(any())).thenReturn(trackJson);

        Aggregate result = aggregateService.aggregate(null, List.of("1", "2"), null);
        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());

        result = aggregateService.aggregate(null, List.of("3", "4", "5"), null);
        assertEquals(trackJson, result.getTrack());
        assertNull(result.getShipments());
        assertNull(result.getPricing());


        verify(trackClient, times(1)).fetchTrack(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);

        verify(trackClient, times(1)).fetchTrack(trackCaptor.capture());
        assertLinesMatch(trackCaptor.getValue(), List.of("1", "2", "3", "4", "5"));
    }

    @Test
    void onlyPricingInThreeRequests() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(pricingClient.fetchPricing(any())).thenReturn(pricingJson);

        Aggregate result = aggregateService.aggregate(List.of("NL", "BE"), null, null);
        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());

        result = aggregateService.aggregate(List.of("RU"), null, null);
        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());

        result = aggregateService.aggregate(List.of("DE", "IE", "LU"), null, null);
        assertEquals(pricingJson, result.getPricing());
        assertNull(result.getShipments());
        assertNull(result.getTrack());

        verify(pricingClient, times(1)).fetchPricing(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);

        verify(pricingClient, times(1)).fetchPricing(priceCaptor.capture());
        assertLinesMatch(priceCaptor.getValue(), List.of("NL", "BE", "RU", "DE", "IE", "LU"));
    }

    @Test
    void aggregateAllShipmentsReachesThresholdFirst() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode pricingJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        JsonNode trackJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        JsonNode shipmentJson = mapper.readTree(readFileAsString("src/test/resources/json/pricing.json"));
        when(pricingClient.fetchPricing(any())).thenReturn(pricingJson);
        when(trackClient.fetchTrack(any())).thenReturn(trackJson);
        when(shipmentClient.fetchShipment(any())).thenReturn(shipmentJson);

        Aggregate result = aggregateService.aggregate(
                List.of("NL", "BE", "RU"),
                List.of("1", "2", "3"),
                List.of("1", "2", "3"));

        assertNull(result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());

        result = aggregateService.aggregate(
                null,
                null,
                List.of("4", "5"));

        assertEquals(shipmentJson, result.getShipments());
        assertNull(result.getPricing());
        assertNull(result.getTrack());

        verify(shipmentClient, times(1)).fetchShipment(any());
        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);

        verify(shipmentClient, times(1)).fetchShipment(shipmentsCaptor.capture());
        assertLinesMatch(shipmentsCaptor.getValue(), List.of("1", "2", "3", "4", "5"));

        //to assert shipment queue is cleaned up, but others are there another call is made
        result = aggregateService.aggregate(
                List.of("DE", "AU"),
                List.of("4", "5"),
                List.of("1"));

        assertNull(result.getShipments());
        assertEquals(pricingJson, result.getPricing());
        assertEquals(trackJson, result.getTrack());

        verify(pricingClient, times(1)).fetchPricing(any());
        verify(trackClient, times(1)).fetchTrack(any());

        verify(pricingClient, times(1)).fetchPricing(priceCaptor.capture());
        assertLinesMatch(priceCaptor.getValue(), List.of("NL", "BE", "RU", "DE", "AU"));

        verify(trackClient, times(1)).fetchTrack(trackCaptor.capture());
        assertLinesMatch(trackCaptor.getValue(), List.of("1", "2", "3", "4", "5"));

        verifyNoMoreInteractions(pricingClient, trackClient, shipmentClient);
    }

}
