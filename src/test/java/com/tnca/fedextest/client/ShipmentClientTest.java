package com.tnca.fedextest.client;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.List;

import static com.tnca.fedextest.TestUtil.readFileAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest(ShipmentClient.class)
class ShipmentClientTest {

    @Autowired
    private ShipmentClient shipmentClient;

    @Autowired
    MockRestServiceServer mockRestServiceServer;

    @Test
    void fetchShipment() throws Exception {

        this.mockRestServiceServer
                .expect(requestTo("http://localhost:8081/shipments?q=109347263,123456891,123456892"))
                .andRespond(withSuccess(readFileAsString("src/test/resources/json/shipment.json"), MediaType.APPLICATION_JSON));

        JsonNode node = shipmentClient.fetchShipment(List.of("109347263", "123456891", "123456892"));

        assertEquals(node.get("109347263").get(0).asText(), "box");
        assertEquals(node.get("123456891").get(0).asText(), "envelope");
        assertEquals(node.get("123456892").get(0).asText(), "envelope");
    }

    @Test
    void shipmentErrorResponse() throws Exception {

        this.mockRestServiceServer
                .expect(requestTo("http://localhost:8081/shipments?q=109347263"))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE));

        assertNull(shipmentClient.fetchShipment(List.of("109347263")));
    }

}