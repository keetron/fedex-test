package com.tnca.fedextest.client;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.List;

import static com.tnca.fedextest.TestUtil.readFileAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest(TrackClient.class)
class TrackClientTest {

    @Autowired
    private TrackClient trackClient;

    @Autowired
    MockRestServiceServer mockRestServiceServer;

    @Test
    void fetchTrack() throws Exception {

        this.mockRestServiceServer
                .expect(requestTo("http://localhost:8081/track?q=109347263,123456891,987655544"))
                .andRespond(withSuccess(readFileAsString("src/test/resources/json/track.json"), MediaType.APPLICATION_JSON));

        JsonNode node = trackClient.fetchTrack(List.of("109347263", "123456891", "987655544"));

        assertEquals(node.get("109347263").asText(), "NEW");
        assertEquals(node.get("123456891").asText(), "DELIVERED");
        assertEquals(node.get("987655544").asText(), "COLLECTING");
    }

    @Test
    void trackErrorResponse() throws Exception {

        this.mockRestServiceServer
                .expect(requestTo("http://localhost:8081/track?q=109347263"))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE));

        assertNull(trackClient.fetchTrack(List.of("109347263")));
    }

}