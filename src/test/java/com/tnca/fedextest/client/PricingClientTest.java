package com.tnca.fedextest.client;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.List;

import static com.tnca.fedextest.TestUtil.readFileAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest(PricingClient.class)
class PricingClientTest {

    @Autowired
    private PricingClient pricingClient;

    @Autowired
    MockRestServiceServer mockRestServiceServer;

    @Test
    void fetchPricing() throws Exception {

        this.mockRestServiceServer
                .expect(requestTo("http://localhost:8081/pricing?q=BE,RU,NL"))
                .andRespond(withSuccess(readFileAsString("src/test/resources/json/pricing.json"), MediaType.APPLICATION_JSON));

        JsonNode node = pricingClient.fetchPricing(List.of("BE", "RU", "NL"));

        assertEquals(node.get("BE").asDouble(), 0.09023803992743717);
        assertEquals(node.get("RU").asDouble(), 13.372667597996546);
        assertEquals(node.get("NL").asDouble(), 41.497417329066934);
    }

    @Test
    void pricingErrorResponse() throws Exception {

        this.mockRestServiceServer
                .expect(requestTo("http://localhost:8081/pricing?q=BE,RU,NL"))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE));

        assertNull(pricingClient.fetchPricing(List.of("BE", "RU", "NL")));
    }
}
