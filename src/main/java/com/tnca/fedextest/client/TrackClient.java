package com.tnca.fedextest.client;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
public class TrackClient {
    private final RestTemplate restTemplate;
    private final String trackHost;

    @Autowired
    public TrackClient(RestTemplateBuilder restTemplateBuilder,
                       @Value("${track.host}") String trackHost) {
        this.restTemplate = restTemplateBuilder.build();
        this.trackHost = trackHost;
    }

    public JsonNode fetchTrack(List<String> track) {
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(trackHost);
        urlBuilder.queryParam("q", String.join(",", track));
        String url = urlBuilder.build(false).encode().toUriString();

        try {
            return restTemplate.getForObject(url, JsonNode.class);
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

}
