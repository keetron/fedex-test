package com.tnca.fedextest.client;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
public class PricingClient {
    private final RestTemplate restTemplate;
    private final String pricingHost;


    @Autowired
    public PricingClient(RestTemplateBuilder restTemplateBuilder,
                         @Value("${pricing.host}") String pricingHost) {
        this.restTemplate = restTemplateBuilder.build();
        this.pricingHost = pricingHost;
    }

    public JsonNode fetchPricing(List<String> pricing) {
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(pricingHost);
        urlBuilder.queryParam("q", String.join(",", pricing));
        String url = urlBuilder.build(false).encode().toUriString();

        try {
            return restTemplate.getForObject(url, JsonNode.class);
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }


}
