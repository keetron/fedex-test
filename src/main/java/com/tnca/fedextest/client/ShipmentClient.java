package com.tnca.fedextest.client;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
public class ShipmentClient {
    private final RestTemplate restTemplate;
    private final String shipmentHost;

    @Autowired
    public ShipmentClient(RestTemplateBuilder restTemplateBuilder,
                          @Value("${shipment.host}") String shipmentHost) {
        this.restTemplate = restTemplateBuilder.build();
        this.shipmentHost = shipmentHost;
    }

    public JsonNode fetchShipment(List<String> shipments) {
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(shipmentHost);
        urlBuilder.queryParam("q", String.join(",", shipments));
        String url = urlBuilder.build(false).encode().toUriString();

        try {
            return restTemplate.getForObject(url, JsonNode.class);
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

}
