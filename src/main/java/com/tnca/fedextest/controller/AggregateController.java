package com.tnca.fedextest.controller;

import com.tnca.fedextest.model.Aggregate;
import com.tnca.fedextest.service.AggregateService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;
import java.util.List;

@RestController
@Validated
public class AggregateController {
    private final AggregateService service;

    public AggregateController(AggregateService service) {
        this.service = service;
    }

    @GetMapping("/aggregation")
    public Aggregate fetchAggregate(
            @RequestParam(required = false) List<@Size(min = 2, max = 2) String> pricing,
            @RequestParam(required = false) List<@Size(min = 9, max = 9) String> track,
            @RequestParam(required = false) List<@Size(min = 9, max = 9) String> shipments
    ) {
        return service.aggregate(pricing, track, shipments);
    }

}
