package com.tnca.fedextest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class AggregatorControllerAdvice {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> constraintViolation(final ConstraintViolationException e) {

        return error(e.getConstraintViolations().toString(), HttpStatus.BAD_REQUEST);

    }

    private ResponseEntity<String> error(final String exception, final HttpStatus httpStatus) {

        final String message = Optional.of(exception).orElse(exception.getClass().getSimpleName());

        return new ResponseEntity<>(message, httpStatus);

    }
}