package com.tnca.fedextest.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.tnca.fedextest.client.PricingClient;
import com.tnca.fedextest.client.ShipmentClient;
import com.tnca.fedextest.client.TrackClient;
import com.tnca.fedextest.model.Aggregate;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
public class AggregateService {
    private final PricingClient pricingClient;
    private final ShipmentClient shipmentClient;
    private final TrackClient trackClient;

    private List<String> trackQueue = Collections.emptyList();
    private List<String> pricingQueue = Collections.emptyList();
    private List<String> shipmentsQueue = Collections.emptyList();

    private long trackTimer = System.currentTimeMillis();
    private long pricingTimer = System.currentTimeMillis();
    private long shipmentTimer = System.currentTimeMillis();

    public AggregateService(PricingClient pricingClient, ShipmentClient shipmentClient, TrackClient trackClient) {
        this.pricingClient = pricingClient;
        this.shipmentClient = shipmentClient;
        this.trackClient = trackClient;
    }

    public Aggregate aggregate(
            List<String> pricing,
            List<String> track,
            List<String> shipments
    ) {
        if (shipments != null)
            shipmentsQueue = Stream.concat(shipmentsQueue.stream(), shipments.stream()).collect(toList());
        if (pricing != null)
            pricingQueue = Stream.concat(pricingQueue.stream(), pricing.stream()).collect(toList());
        if (track != null)
            trackQueue = Stream.concat(trackQueue.stream(), track.stream()).collect(toList());

        return new Aggregate(
                getPricing(),
                getTrack(),
                getShipment());

    }

    private boolean timerOverFiveSeconds(long timer) {
        long currentTime = System.currentTimeMillis();
        return currentTime - timer >= 5000;
    }

    private JsonNode getShipment() {
        if (shipmentsQueue == null) return null;
        if (!shipmentsQueue.isEmpty() && (shipmentsQueue.size() >= 5 || timerOverFiveSeconds(shipmentTimer))) {
            JsonNode result = shipmentClient.fetchShipment(shipmentsQueue);
            shipmentsQueue = List.of();
            return result;
        }
        return null;
    }

    private JsonNode getPricing() {
        if (pricingQueue == null) return null;
        if (!pricingQueue.isEmpty() && (pricingQueue.size() >= 5 || timerOverFiveSeconds(pricingTimer))) {
            JsonNode result = pricingClient.fetchPricing(pricingQueue);
            pricingQueue = List.of();
            return result;
        }
        return null;
    }

    private JsonNode getTrack() {
        if (trackQueue == null) return null;
        if (!trackQueue.isEmpty() && (trackQueue.size() >= 5 || timerOverFiveSeconds(trackTimer))) {
            JsonNode result = trackClient.fetchTrack(trackQueue);
            trackQueue = List.of();
            return result;
        }
        return null;
    }
}
