package com.tnca.fedextest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FedexTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FedexTestApplication.class, args);
	}

}
