package com.tnca.fedextest.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Aggregate {
    private JsonNode pricing;
    private JsonNode track;
    private JsonNode shipments;
}
