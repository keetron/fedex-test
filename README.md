#FEDEX AGGREGATOR

This is a demo project to showcase how I work, what code can be expected from me.
Something to keep in mind is that I rather finish half and have it work well over doing everything but being buggy or not meeting functional requirements.
Considering this assignment was set at 8 hours, this means it is not complete.

##Technical approach
Spring Boot 2.1 as the most recent stable. Java 11 as that is the latest version I worked extensively with. I would prefer Kotlin for dataclasses, immutability and general QOL improvements (such as greatly improved readability) but it would seem Java 14 is heading in the right direction.

My normal approach is test driven, which means I set up the controller, service and repository/client classes empty and go from there. Writing tests first allows me to capture the functional requirements in tests before writing the code. I do this by setting up empty tests with just the method names in place, filling and resolving them one by one.

The test levels I use are Unit Tests against service classes, Module tests against the controller and client classes using the Spring testing framework and Integration Tests against the full application and Wiremock although in this case the expectation is to have a running stub container.

##Running the tests
First start the docker container using the commandline: `docker run -p 8081:8080 xyzassessment/backend-services`

Make sure you have java 11 installed on your machine and set up as the current active version.

Then either run all the tests using Intellij or on the commandline (in a new terminal window): `./mvnw clean verify`
##Running the application
First start the docker container using the commandline: `docker run -p 8081:8080 xyzassessment/backend-services`

Then either run the `FedexTestAPplication` class in Intellij or use the command line: `./mvnw spring-boot:run`

##Stories
###AS-1
`As TNT, I want to be able to query all services in a single network call to optimise network traffic.`

Nothing special here, I choose to avoid deserializing the response objects from the container by putting them in a simple JsonNode. As a general principle, I do not think an aggregator should perform any logic on the response objects and using a JsonNode supports that. 

###AS-2
`as TNT, I want service calls to be throttled and bulked into consolidated requests per respective API to prevent services from being overloaded.`

Here the question is how many clients will be using the aggregator. If a single client that will fire of requests until it gets a response, that is fine, you can pile up the requests while returning null and return all at the same time when the limit for any type is reached. Not elegant but functional.
With multiple clients, which would be more likely in a real world scenario, some invocation of session tracking, request tracking and stub response deconstruction will need to take place.

Due to my personal time constraints, the first approach is taken where the client will get a response for the request that makes any of the three types meet 5 and this response will contain ALL three types. This to allow the client to continue processing as soon as a response is received.
Also I really do not like using class variables like I did in the `AggregateService`, it will make for a hard to expand and debug solution. A solution of something like SQS from AWS or pubsub from GCP would be much better but I ran out of time to improve now.
As a bonus, using SQS or pubsub would make it much easier to run this service in a multi-instance cloud environment.

###AS-3
`as TNT, I want service calls to be scheduled periodically even if the queue is not full to prevent overly-long response times.`
As I ran out of time, I did a really quick and dirty implementation. I would rather do a more elegant solution with a queueing system.

##Things I would like to add that was either no time for or not specified
* A linter such as checkstyle ran when building
* Test coverage tooling, jacoco would work fine, also ran while building
* Let's go fancy: mutation testing with pitest
* WireMock, I do not like my tests to be dependent on elements outside the build and rather see them be self-contained
* Real queueing system or messaging system instead of the current clunky implementation
* logging, the error handling on the clients is rudimentary at best and some proper logging would be fitting here.